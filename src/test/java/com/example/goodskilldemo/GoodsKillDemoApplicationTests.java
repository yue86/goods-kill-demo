package com.example.goodskilldemo;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import org.springframework.test.context.event.annotation.PrepareTestInstance;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
class GoodsKillDemoApplicationTests {

    MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;


    @BeforeEach
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void contextLoads() throws Exception {
        ExecutorService pool = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            MyThread myThread = new MyThread(i, "线程：" + i + "");
            pool.execute(myThread);
        }
        pool.shutdown();
//        String responseString = mvc.perform
//                //http://192.168.3.99:9999/
//                        (MockMvcRequestBuilders
//                                        .post("http://192.168.3.99:9999/seckill/seckillExcution/1/" + "1" + "/c4ca4238a0b923820dcc509a6f75849b")
//                                        .contentType(MediaType.APPLICATION_JSON)
////                        .content()设置请求body内容
//                        )
//                .andReturn().getResponse().getContentAsString();
//        System.out.println(responseString);

    }

    class MyThread implements Runnable {

        private int userId;
        private String threadName;

        public MyThread(int userId, String threadName) {
            this.userId = userId;
            this.threadName = threadName;
        }

        @Override
        public void run() {
            String responseString = null;
            try {
                responseString = mvc.perform
                        //http://192.168.3.99:9999/
                                (MockMvcRequestBuilders
                                                .post("http://192.168.3.99:9999/seckill/seckillExcution/1/" + userId + "/c4ca4238a0b923820dcc509a6f75849b")
                                                .contentType(MediaType.APPLICATION_JSON)
                                        //                        .content()设置请求body内容
                                )
                        .andReturn().getResponse().getContentAsString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(threadName + responseString);
        }
    }
}
