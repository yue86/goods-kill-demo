package com.example.goodskilldemo.service;

import com.example.goodskilldemo.http.Result;
import com.example.goodskilldemo.model.GoodsSeckill;

public interface IGoodsSeckillService {
    Result selectSeckillGoodsList();

    Result exposerUrl(Long seckillId);

    Result selectSeckillGoodDetail(Long seckillId);

    Result executeSeckill(Long seckillId, Long userId);

    boolean save(GoodsSeckill goodsSeckill);

    void hotData();
}
