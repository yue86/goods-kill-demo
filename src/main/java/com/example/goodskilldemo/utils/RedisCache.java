package com.example.goodskilldemo.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisCache {
    @Autowired
    private RedisTemplate redisTemplate;


    public Long increment(final String key) {
        return redisTemplate.opsForValue().increment(key);
    }

    public Long decrement(final String key) {
        return redisTemplate.opsForValue().decrement(key);
    }

    public Long luaDecrement(final String key) {
        RedisScript<Long> redisScript = new DefaultRedisScript<>(buildLuaDecrScript(), Long.class);
        Number execute = (Number) redisTemplate.execute(redisScript, Collections.singletonList(key));
        return execute.longValue();
    }

    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public <T> void setCacheObject(final String key, final T value, final Integer timeout, final TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }

    public boolean expire(final String key, final Long timeout, final TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    public <T> T getCacheObject(final String key) {
        ValueOperations<String, T> valueOperations = redisTemplate.opsForValue();
        return valueOperations.get(key);
    }

    public boolean deleteCacheObject(final String key) {
        return redisTemplate.delete(key);
    }

    public long deleteObject(final Collection<Object> collection) {
        return redisTemplate.delete(collection);
    }

    /**
     * 删除指定前缀的key
     */
    public long deleteLikesKeyObject(String prefix) {
        return redisTemplate.delete(getLikesKeyList(prefix));
    }


    /**
     * 获取指定前缀键值对
     */
    private Collection getLikesKeyList(String prefix) {
        //获取所有key
        Set<String> keys = redisTemplate.keys(prefix);

        //批量获取数据
        return redisTemplate.opsForValue().multiGet(keys);
    }


    /**
     * 缓存Set
     *
     * @param key   缓存键值
     * @param value 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> long setCacheSet(final String key, final Object value) {
        Long count = redisTemplate.opsForSet().add(key, value);
        return count == null ? 0 : count;
    }


    /**
     * 判断key-set中是否存在value
     *
     * @param key   缓存键值
     * @param value 缓存的数据
     * @return boolean
     */
    public Boolean catainsCacheSet(final String key, final Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }


    /**
     * 缓存Map
     *
     * @param key     redis键
     * @param dataMap map对象
     */
    public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
        if (dataMap != null) {
            redisTemplate.opsForHash().putAll(key, dataMap);
        }
    }


    private String buildLuaDecrScript() {
        return "local c" +
                "\nc = redis.call('get',KEYS[1])" +
                "\nif c and tonumber(c) < 0 then" +
                "\nreturn c;" +
                "\nend" +
                "\nc = redis.call('decr',KEYS[1])" +
                "\nreturn c;";
    }
}
