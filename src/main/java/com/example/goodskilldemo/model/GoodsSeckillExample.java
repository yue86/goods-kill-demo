package com.example.goodskilldemo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodsSeckillExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public GoodsSeckillExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSeckillIdIsNull() {
            addCriterion("seckill_id is null");
            return (Criteria) this;
        }

        public Criteria andSeckillIdIsNotNull() {
            addCriterion("seckill_id is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillIdEqualTo(Long value) {
            addCriterion("seckill_id =", value, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdNotEqualTo(Long value) {
            addCriterion("seckill_id <>", value, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdGreaterThan(Long value) {
            addCriterion("seckill_id >", value, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdGreaterThanOrEqualTo(Long value) {
            addCriterion("seckill_id >=", value, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdLessThan(Long value) {
            addCriterion("seckill_id <", value, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdLessThanOrEqualTo(Long value) {
            addCriterion("seckill_id <=", value, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdIn(List<Long> values) {
            addCriterion("seckill_id in", values, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdNotIn(List<Long> values) {
            addCriterion("seckill_id not in", values, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdBetween(Long value1, Long value2) {
            addCriterion("seckill_id between", value1, value2, "seckillId");
            return (Criteria) this;
        }

        public Criteria andSeckillIdNotBetween(Long value1, Long value2) {
            addCriterion("seckill_id not between", value1, value2, "seckillId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNull() {
            addCriterion("goods_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNotNull() {
            addCriterion("goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdEqualTo(Long value) {
            addCriterion("goods_id =", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotEqualTo(Long value) {
            addCriterion("goods_id <>", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThan(Long value) {
            addCriterion("goods_id >", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThanOrEqualTo(Long value) {
            addCriterion("goods_id >=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThan(Long value) {
            addCriterion("goods_id <", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThanOrEqualTo(Long value) {
            addCriterion("goods_id <=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIn(List<Long> values) {
            addCriterion("goods_id in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotIn(List<Long> values) {
            addCriterion("goods_id not in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdBetween(Long value1, Long value2) {
            addCriterion("goods_id between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotBetween(Long value1, Long value2) {
            addCriterion("goods_id not between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceIsNull() {
            addCriterion("seckill_price is null");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceIsNotNull() {
            addCriterion("seckill_price is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceEqualTo(Integer value) {
            addCriterion("seckill_price =", value, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceNotEqualTo(Integer value) {
            addCriterion("seckill_price <>", value, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceGreaterThan(Integer value) {
            addCriterion("seckill_price >", value, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("seckill_price >=", value, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceLessThan(Integer value) {
            addCriterion("seckill_price <", value, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceLessThanOrEqualTo(Integer value) {
            addCriterion("seckill_price <=", value, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceIn(List<Integer> values) {
            addCriterion("seckill_price in", values, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceNotIn(List<Integer> values) {
            addCriterion("seckill_price not in", values, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceBetween(Integer value1, Integer value2) {
            addCriterion("seckill_price between", value1, value2, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("seckill_price not between", value1, value2, "seckillPrice");
            return (Criteria) this;
        }

        public Criteria andSeckillNumIsNull() {
            addCriterion("seckill_num is null");
            return (Criteria) this;
        }

        public Criteria andSeckillNumIsNotNull() {
            addCriterion("seckill_num is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillNumEqualTo(Integer value) {
            addCriterion("seckill_num =", value, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumNotEqualTo(Integer value) {
            addCriterion("seckill_num <>", value, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumGreaterThan(Integer value) {
            addCriterion("seckill_num >", value, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("seckill_num >=", value, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumLessThan(Integer value) {
            addCriterion("seckill_num <", value, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumLessThanOrEqualTo(Integer value) {
            addCriterion("seckill_num <=", value, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumIn(List<Integer> values) {
            addCriterion("seckill_num in", values, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumNotIn(List<Integer> values) {
            addCriterion("seckill_num not in", values, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumBetween(Integer value1, Integer value2) {
            addCriterion("seckill_num between", value1, value2, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillNumNotBetween(Integer value1, Integer value2) {
            addCriterion("seckill_num not between", value1, value2, "seckillNum");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusIsNull() {
            addCriterion("seckill_status is null");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusIsNotNull() {
            addCriterion("seckill_status is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusEqualTo(Boolean value) {
            addCriterion("seckill_status =", value, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusNotEqualTo(Boolean value) {
            addCriterion("seckill_status <>", value, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusGreaterThan(Boolean value) {
            addCriterion("seckill_status >", value, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusGreaterThanOrEqualTo(Boolean value) {
            addCriterion("seckill_status >=", value, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusLessThan(Boolean value) {
            addCriterion("seckill_status <", value, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusLessThanOrEqualTo(Boolean value) {
            addCriterion("seckill_status <=", value, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusIn(List<Boolean> values) {
            addCriterion("seckill_status in", values, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusNotIn(List<Boolean> values) {
            addCriterion("seckill_status not in", values, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusBetween(Boolean value1, Boolean value2) {
            addCriterion("seckill_status between", value1, value2, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillStatusNotBetween(Boolean value1, Boolean value2) {
            addCriterion("seckill_status not between", value1, value2, "seckillStatus");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginIsNull() {
            addCriterion("seckill_begin is null");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginIsNotNull() {
            addCriterion("seckill_begin is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginEqualTo(Date value) {
            addCriterion("seckill_begin =", value, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginNotEqualTo(Date value) {
            addCriterion("seckill_begin <>", value, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginGreaterThan(Date value) {
            addCriterion("seckill_begin >", value, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginGreaterThanOrEqualTo(Date value) {
            addCriterion("seckill_begin >=", value, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginLessThan(Date value) {
            addCriterion("seckill_begin <", value, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginLessThanOrEqualTo(Date value) {
            addCriterion("seckill_begin <=", value, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginIn(List<Date> values) {
            addCriterion("seckill_begin in", values, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginNotIn(List<Date> values) {
            addCriterion("seckill_begin not in", values, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginBetween(Date value1, Date value2) {
            addCriterion("seckill_begin between", value1, value2, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillBeginNotBetween(Date value1, Date value2) {
            addCriterion("seckill_begin not between", value1, value2, "seckillBegin");
            return (Criteria) this;
        }

        public Criteria andSeckillEndIsNull() {
            addCriterion("seckill_end is null");
            return (Criteria) this;
        }

        public Criteria andSeckillEndIsNotNull() {
            addCriterion("seckill_end is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillEndEqualTo(Date value) {
            addCriterion("seckill_end =", value, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndNotEqualTo(Date value) {
            addCriterion("seckill_end <>", value, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndGreaterThan(Date value) {
            addCriterion("seckill_end >", value, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndGreaterThanOrEqualTo(Date value) {
            addCriterion("seckill_end >=", value, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndLessThan(Date value) {
            addCriterion("seckill_end <", value, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndLessThanOrEqualTo(Date value) {
            addCriterion("seckill_end <=", value, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndIn(List<Date> values) {
            addCriterion("seckill_end in", values, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndNotIn(List<Date> values) {
            addCriterion("seckill_end not in", values, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndBetween(Date value1, Date value2) {
            addCriterion("seckill_end between", value1, value2, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillEndNotBetween(Date value1, Date value2) {
            addCriterion("seckill_end not between", value1, value2, "seckillEnd");
            return (Criteria) this;
        }

        public Criteria andSeckillRankIsNull() {
            addCriterion("seckill_rank is null");
            return (Criteria) this;
        }

        public Criteria andSeckillRankIsNotNull() {
            addCriterion("seckill_rank is not null");
            return (Criteria) this;
        }

        public Criteria andSeckillRankEqualTo(Integer value) {
            addCriterion("seckill_rank =", value, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankNotEqualTo(Integer value) {
            addCriterion("seckill_rank <>", value, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankGreaterThan(Integer value) {
            addCriterion("seckill_rank >", value, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankGreaterThanOrEqualTo(Integer value) {
            addCriterion("seckill_rank >=", value, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankLessThan(Integer value) {
            addCriterion("seckill_rank <", value, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankLessThanOrEqualTo(Integer value) {
            addCriterion("seckill_rank <=", value, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankIn(List<Integer> values) {
            addCriterion("seckill_rank in", values, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankNotIn(List<Integer> values) {
            addCriterion("seckill_rank not in", values, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankBetween(Integer value1, Integer value2) {
            addCriterion("seckill_rank between", value1, value2, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andSeckillRankNotBetween(Integer value1, Integer value2) {
            addCriterion("seckill_rank not between", value1, value2, "seckillRank");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIsNull() {
            addCriterion("is_deleted is null");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIsNotNull() {
            addCriterion("is_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeletedEqualTo(Boolean value) {
            addCriterion("is_deleted =", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotEqualTo(Boolean value) {
            addCriterion("is_deleted <>", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedGreaterThan(Boolean value) {
            addCriterion("is_deleted >", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_deleted >=", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedLessThan(Boolean value) {
            addCriterion("is_deleted <", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedLessThanOrEqualTo(Boolean value) {
            addCriterion("is_deleted <=", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIn(List<Boolean> values) {
            addCriterion("is_deleted in", values, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotIn(List<Boolean> values) {
            addCriterion("is_deleted not in", values, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedBetween(Boolean value1, Boolean value2) {
            addCriterion("is_deleted between", value1, value2, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_deleted not between", value1, value2, "isDeleted");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}