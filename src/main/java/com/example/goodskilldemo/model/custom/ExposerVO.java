package com.example.goodskilldemo.model.custom;

import com.example.goodskilldemo.constants.SeckillStatusEnum;

/**
 * 秒杀服务接口地址暴露类
 */
public class ExposerVO {
    private static final long serialVersionUID = -7615136662052646516L;

    //秒杀状态枚举
    private SeckillStatusEnum seckillStatusEnum;

    //加密措施
    private String md5;

    private Long seckillId;


    //系统当前时间
    private String nowTime;

    //秒杀开始时间
    private String startTime;

    //秒杀结束时间
    private String endTime;

    public ExposerVO(SeckillStatusEnum seckillStatusEnum, String md5, Long seckillId) {
        this.seckillStatusEnum = seckillStatusEnum;
        this.md5 = md5;
        this.seckillId = seckillId;
    }

    public ExposerVO(SeckillStatusEnum seckillStatusEnum, Long seckillId, String nowTime, String startTime, String endTime) {
        this.seckillStatusEnum = seckillStatusEnum;
        this.seckillId = seckillId;
        this.nowTime = nowTime;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public ExposerVO(SeckillStatusEnum seckillStatusEnum, Long seckillId) {
        this.seckillStatusEnum = seckillStatusEnum;
        this.seckillId = seckillId;
    }

    public SeckillStatusEnum getSeckillStatusEnum() {
        return seckillStatusEnum;
    }

    public void setSeckillStatusEnum(SeckillStatusEnum seckillStatusEnum) {
        this.seckillStatusEnum = seckillStatusEnum;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public Long getSeckillId() {
        return seckillId;
    }

    public void setSeckillId(Long seckillId) {
        this.seckillId = seckillId;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
