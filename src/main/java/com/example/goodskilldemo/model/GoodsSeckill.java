package com.example.goodskilldemo.model;

import java.io.Serializable;
import java.util.Date;

/**
 * goods_seckill
 * @author 
 */
public class GoodsSeckill implements Serializable {
    /**
     * 自增ID
     */
    private Long seckillId;

    /**
     * 秒杀商品ID
     */
    private Long goodsId;

    /**
     * 秒杀价格
     */
    private Integer seckillPrice;

    /**
     * 秒杀数量
     */
    private Integer seckillNum;

    /**
     * 秒杀商品状态（0下架，1上架）
     */
    private Boolean seckillStatus;

    /**
     * 秒杀开始时间
     */
    private Date seckillBegin;

    /**
     * 秒杀结束时间
     */
    private Date seckillEnd;

    /**
     * 秒杀商品排序
     */
    private Integer seckillRank;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 删除标识字段(0-未删除 1-已删除)
     */
    private Boolean isDeleted;

    private static final long serialVersionUID = 1L;

    public Long getSeckillId() {
        return seckillId;
    }

    public void setSeckillId(Long seckillId) {
        this.seckillId = seckillId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSeckillPrice() {
        return seckillPrice;
    }

    public void setSeckillPrice(Integer seckillPrice) {
        this.seckillPrice = seckillPrice;
    }

    public Integer getSeckillNum() {
        return seckillNum;
    }

    public void setSeckillNum(Integer seckillNum) {
        this.seckillNum = seckillNum;
    }

    public Boolean getSeckillStatus() {
        return seckillStatus;
    }

    public void setSeckillStatus(Boolean seckillStatus) {
        this.seckillStatus = seckillStatus;
    }

    public Date getSeckillBegin() {
        return seckillBegin;
    }

    public void setSeckillBegin(Date seckillBegin) {
        this.seckillBegin = seckillBegin;
    }

    public Date getSeckillEnd() {
        return seckillEnd;
    }

    public void setSeckillEnd(Date seckillEnd) {
        this.seckillEnd = seckillEnd;
    }

    public Integer getSeckillRank() {
        return seckillRank;
    }

    public void setSeckillRank(Integer seckillRank) {
        this.seckillRank = seckillRank;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        GoodsSeckill other = (GoodsSeckill) that;
        return (this.getSeckillId() == null ? other.getSeckillId() == null : this.getSeckillId().equals(other.getSeckillId()))
            && (this.getGoodsId() == null ? other.getGoodsId() == null : this.getGoodsId().equals(other.getGoodsId()))
            && (this.getSeckillPrice() == null ? other.getSeckillPrice() == null : this.getSeckillPrice().equals(other.getSeckillPrice()))
            && (this.getSeckillNum() == null ? other.getSeckillNum() == null : this.getSeckillNum().equals(other.getSeckillNum()))
            && (this.getSeckillStatus() == null ? other.getSeckillStatus() == null : this.getSeckillStatus().equals(other.getSeckillStatus()))
            && (this.getSeckillBegin() == null ? other.getSeckillBegin() == null : this.getSeckillBegin().equals(other.getSeckillBegin()))
            && (this.getSeckillEnd() == null ? other.getSeckillEnd() == null : this.getSeckillEnd().equals(other.getSeckillEnd()))
            && (this.getSeckillRank() == null ? other.getSeckillRank() == null : this.getSeckillRank().equals(other.getSeckillRank()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getSeckillId() == null) ? 0 : getSeckillId().hashCode());
        result = prime * result + ((getGoodsId() == null) ? 0 : getGoodsId().hashCode());
        result = prime * result + ((getSeckillPrice() == null) ? 0 : getSeckillPrice().hashCode());
        result = prime * result + ((getSeckillNum() == null) ? 0 : getSeckillNum().hashCode());
        result = prime * result + ((getSeckillStatus() == null) ? 0 : getSeckillStatus().hashCode());
        result = prime * result + ((getSeckillBegin() == null) ? 0 : getSeckillBegin().hashCode());
        result = prime * result + ((getSeckillEnd() == null) ? 0 : getSeckillEnd().hashCode());
        result = prime * result + ((getSeckillRank() == null) ? 0 : getSeckillRank().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", seckillId=").append(seckillId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", seckillPrice=").append(seckillPrice);
        sb.append(", seckillNum=").append(seckillNum);
        sb.append(", seckillStatus=").append(seckillStatus);
        sb.append(", seckillBegin=").append(seckillBegin);
        sb.append(", seckillEnd=").append(seckillEnd);
        sb.append(", seckillRank=").append(seckillRank);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}