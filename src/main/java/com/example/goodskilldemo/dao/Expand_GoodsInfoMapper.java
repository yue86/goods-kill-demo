package com.example.goodskilldemo.dao;

import com.example.goodskilldemo.model.GoodsInfo;
import com.example.goodskilldemo.model.GoodsInfoExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface Expand_GoodsInfoMapper extends GoodsInfoMapper{
    List<GoodsInfo> selectByIds(List<Long> list);
}