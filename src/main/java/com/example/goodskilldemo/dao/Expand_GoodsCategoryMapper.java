package com.example.goodskilldemo.dao;

import com.example.goodskilldemo.model.GoodsCategory;
import com.example.goodskilldemo.model.GoodsCategoryExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Expand_GoodsCategoryMapper extends GoodsCategoryMapper{
}