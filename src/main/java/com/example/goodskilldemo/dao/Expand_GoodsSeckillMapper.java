package com.example.goodskilldemo.dao;

import com.example.goodskilldemo.model.GoodsSeckill;
import com.example.goodskilldemo.model.GoodsSeckillExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface Expand_GoodsSeckillMapper extends GoodsSeckillMapper{
    List<GoodsSeckill> selectHomeSeckillListPage();

    void killGoodsByProcedure(HashMap<String, Object> map);

    List<GoodsSeckill> selectHotData();

}