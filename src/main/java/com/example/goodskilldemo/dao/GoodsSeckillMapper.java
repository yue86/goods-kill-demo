package com.example.goodskilldemo.dao;

import com.example.goodskilldemo.model.GoodsSeckill;
import com.example.goodskilldemo.model.GoodsSeckillExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsSeckillMapper {
    long countByExample(GoodsSeckillExample example);

    int deleteByExample(GoodsSeckillExample example);

    int deleteByPrimaryKey(Long seckillId);

    int insert(GoodsSeckill record);

    int insertSelective(GoodsSeckill record);

    List<GoodsSeckill> selectByExample(GoodsSeckillExample example);

    GoodsSeckill selectByPrimaryKey(Long seckillId);

    int updateByExampleSelective(@Param("record") GoodsSeckill record, @Param("example") GoodsSeckillExample example);

    int updateByExample(@Param("record") GoodsSeckill record, @Param("example") GoodsSeckillExample example);

    int updateByPrimaryKeySelective(GoodsSeckill record);

    int updateByPrimaryKey(GoodsSeckill record);
}