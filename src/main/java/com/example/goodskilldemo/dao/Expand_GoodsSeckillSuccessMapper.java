package com.example.goodskilldemo.dao;

import com.example.goodskilldemo.model.GoodsSeckillSuccess;
import com.example.goodskilldemo.model.GoodsSeckillSuccessExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Expand_GoodsSeckillSuccessMapper extends GoodsSeckillSuccessMapper{
    GoodsSeckillSuccess getSeckillSuccessByUserIdAndSeckillId(@Param("userId") Long userId,@Param("seckillId") Long seckillId);

}