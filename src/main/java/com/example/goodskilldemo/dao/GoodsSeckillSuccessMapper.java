package com.example.goodskilldemo.dao;

import com.example.goodskilldemo.model.GoodsSeckillSuccess;
import com.example.goodskilldemo.model.GoodsSeckillSuccessExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsSeckillSuccessMapper {
    long countByExample(GoodsSeckillSuccessExample example);

    int deleteByExample(GoodsSeckillSuccessExample example);

    int deleteByPrimaryKey(Long secId);

    int insert(GoodsSeckillSuccess record);

    int insertSelective(GoodsSeckillSuccess record);

    List<GoodsSeckillSuccess> selectByExample(GoodsSeckillSuccessExample example);

    GoodsSeckillSuccess selectByPrimaryKey(Long secId);

    int updateByExampleSelective(@Param("record") GoodsSeckillSuccess record, @Param("example") GoodsSeckillSuccessExample example);

    int updateByExample(@Param("record") GoodsSeckillSuccess record, @Param("example") GoodsSeckillSuccessExample example);

    int updateByPrimaryKeySelective(GoodsSeckillSuccess record);

    int updateByPrimaryKey(GoodsSeckillSuccess record);
}