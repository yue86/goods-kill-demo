package com.example.goodskilldemo.constants;

public enum SeckillStatusEnum {
    //未到开始时间
    NOT_START,
    //到了秒杀开始时间
    START,
    //到了秒杀开始时间，但是库存不足
    STARTED_SHORTAGE_STOCK,
    //秒杀已经结束
    ALREADY_END
    ;

}
